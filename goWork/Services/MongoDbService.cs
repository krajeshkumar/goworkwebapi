﻿using goWork.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goWork.Services
{
    public class MongoDbService
    {
        public IMongoCollection<Employee> EmployeeCollection { get; set; }
        public IMongoCollection<Questionnaire> QuestionnaireCollection { get; set; }

        public MongoDbService(string databaseName, string collectionName, string databaseUrl)
        {
            var mongoClient = new MongoClient(databaseUrl);
            var mongoDb = mongoClient.GetDatabase(databaseName);
            EmployeeCollection = mongoDb.GetCollection<Employee>(collectionName);
            QuestionnaireCollection = mongoDb.GetCollection<Questionnaire>(collectionName);
        }

        public async Task InsertEmployee(Employee employee) => await EmployeeCollection.InsertOneAsync(employee);

        public async Task InsertQuestionnaire(Questionnaire questionnaire) => await QuestionnaireCollection.InsertOneAsync(questionnaire);

        public async Task<List<Employee>> GetAllEmployees()
        {
            var employees = new List<Employee>();
            var allDoc = await EmployeeCollection.FindAsync(new BsonDocument());
            await allDoc.ForEachAsync(doc => employees.Add(doc));

            return employees;
        }
    }
}
