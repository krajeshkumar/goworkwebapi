﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using goWork.Models;
using goWork.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace goWork.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class SelfReportingController : ControllerBase
    {
        private readonly ILogger<SelfReportingController> _logger;

        public SelfReportingController(ILogger<SelfReportingController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<List<Employee>> Get()
        {
            var mongoService = new MongoDbService("goWork", "Employee", "mongodb://localhost:27017");
            var allEmployees = await mongoService.GetAllEmployees();
            return allEmployees;
        }

        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        public async Task Post(Questionnaire questionnaire)
        {
            //var mongoService = new MongoDbService("goWork", "Questionnaire", "mongodb://localhost:27017");
            //await mongoService.InsertQuestionnaire(questionnaire);
        }
    }
}
