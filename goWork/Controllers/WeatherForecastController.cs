﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using goWork.Models;
using goWork.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace goWork.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public Task<List<Employee>> Get()
        {
            var mongoService = new MongoDbService("goWork", "Employee", "mongodb://localhost:27017");
            var allEmployees = mongoService.GetAllEmployees();
            return allEmployees;
        }

        [HttpPost]
        public async Task Post([FromBody]Employee employee)
        {
            var mongoService = new MongoDbService("goWork", "Employee", "mongodb://localhost:27017");
            await mongoService.InsertEmployee(employee);
        }
    }
}
