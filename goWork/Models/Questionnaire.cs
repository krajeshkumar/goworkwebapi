﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goWork.Models
{
    public class Questionnaire
    {
        [BsonId]
        public ObjectId QuestionId { get; set; }
        [BsonDateTimeOptions(DateOnly = false, Kind = DateTimeKind.Local, Representation = BsonType.DateTime)]
        public DateTime? EntryTime { get; set; }
        [BsonRequired]
        public string ComingFrom { get; set; }
        [BsonRequired]
        public string ModeOfTransport { get; set; }
        [BsonRequired]
        public bool IsStayingInContainmentZone { get; set; }
        public string TravelModeofTransport { get; set; }
        public bool HasTravelledWithUnknownPeople { get; set; }
        public bool HasRecentTravelHistoryInState { get; set; }
        [BsonDateTimeOptions(DateOnly = true, Kind = DateTimeKind.Local, Representation = BsonType.DateTime)]
        public DateTime? TravelStartDate { get; set; }
        [BsonDateTimeOptions(DateOnly = true, Kind = DateTimeKind.Local, Representation = BsonType.DateTime)]
        public DateTime? TravelEndDate { get; set; }
        public bool HasRecentHospitalVisit { get; set; }
        [BsonDateTimeOptions(DateOnly = true, Kind = DateTimeKind.Local, Representation = BsonType.DateTime)]
        public DateTime? HospitalVisitDate { get; set; }
        public string ReasonForHospitalVisit { get; set; }
        public bool HasRecentTravelHistory { get; set; }
        public bool HasRecentVisitor { get; set; }
        [BsonDateTimeOptions(DateOnly = true, Kind = DateTimeKind.Local, Representation = BsonType.DateTime)]
        public DateTime? VisitorArrivalDate { get; set; }
        public string VisitorCity { get; set; }
        public bool HasFever { get; set; }
        public bool HasMask { get; set; }
        public bool HasCough { get; set; }
        public bool HasChestPain { get; set; }
        public decimal Temperature { get; set; }
    }
}
