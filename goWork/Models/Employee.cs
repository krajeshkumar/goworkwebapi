﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace goWork.Models
{
    public class Employee
    {
        [BsonId]
        public ObjectId EmpId { get; set; }
        [BsonRequired]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
        public string Team { get; set; }
    }
}
