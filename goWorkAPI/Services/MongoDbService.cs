﻿using goWorkAPI.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace goWork.Services
{
    public class MongoDbService
    {
        private readonly IMongoCollection<Employee> _employeeCollection;

        public MongoDbService(IGoWorkDatabaseSettings settings)
        {
            var mongoClient = new MongoClient(settings.ConnectionString);
            var mongoDb = mongoClient.GetDatabase(settings.DatabaseName);
            _employeeCollection = mongoDb.GetCollection<Employee>(settings.CollectionName);
        }

        public async Task<List<Employee>> GetAllEmployees()
        {
            var employees = new List<Employee>();
            var allDoc = await _employeeCollection.FindAsync(new BsonDocument());
            await allDoc.ForEachAsync(doc => employees.Add(doc));

            return employees;
        }

        public Employee Get(string id) =>
            _employeeCollection.Find<Employee>(employee => employee.Id == id).FirstOrDefault();

        public Employee Create(Employee employee)
        {
            _employeeCollection.InsertOne(employee);
            return employee;
        }

        public void Update(string id, Employee employeeIn) =>
            _employeeCollection.ReplaceOne(employee => employee.Id == id, employeeIn);

        public void Remove(Employee employeeIn) =>
            _employeeCollection.DeleteOne(employee => employee.Id == employeeIn.Id);

        public void Remove(string id) =>
            _employeeCollection.DeleteOne(employee => employee.Id == id);
    }
}
