﻿using goWork.Services;
using goWorkAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace goWorkAPI.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class SelfReportingController : ControllerBase
    {
        private readonly ILogger<SelfReportingController> _logger;
        private readonly MongoDbService _mongoDbService;

        public SelfReportingController(ILogger<SelfReportingController> logger, MongoDbService mongoDbService)
        {
            _logger = logger;
            _mongoDbService = mongoDbService;
        }

        [HttpGet]
        public async Task<List<Employee>> Get()
        {
            var allEmployees = await _mongoDbService.GetAllEmployees();
            return allEmployees;
        }

        //[HttpGet]
        //public string Get()
        //{
        //    return "value";
        //}

        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value = " + id;
        }

        [HttpPost]
        public async Task Post(Questionnaire questionnaire)
        {
            //await _mongoDbService.Create(employee);
        }
    }
}
